Gem::Specification.new do |s|
  s.name      = 'altair'
  s.summary   = 'Basic document validation'
  s.version   = '0.1.4'
  s.license   = 'BSD-2-Clause'
  s.authors   = ['Felipe Cabrera']
  s.email     = 'fecabrera0@outlook.com'
  s.files     = `git ls-files lib`.split(/\n/)
  s.homepage  = 'https://gitlab.com/armizh/altair'

  s.add_dependency 'indifferent-hash'
end