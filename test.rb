$:.unshift File.join(__dir__, 'lib')

require 'date'
require 'altair'

Altair.config

schema = Altair::Schema.new({
  id: Integer,
  email: { type: String, required: true },
  name: {
    first: String,
    last: String
  },
  birthday: { type: Date, max: ->{ Date.today.prev_year(18) } }
})

data = IndifferentHash.from_hash({
  id: 1,
  email: 'jsmith@gmail.com',
  name: {
    first: 'John',
    last: 'Smith'
  },
  birthday: '1995-12-02'
})

p doc

doc.validate!