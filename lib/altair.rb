require 'boolean'
require 'date'
require 'altair/field'
require 'altair/document'
require 'altair/schema'
require 'altair/validation_error'
require 'altair/conversion_error'

module Altair
  attr_accessor :converter, :validators

  def config(options = {})
    @validators = options[:validators] || default_validators
    @converter = options[:converter] || default_converter
  end

  def define_validator(name, &block)
    validators[name] = block
  end

  def default_validators
    {
      required: Proc.new { |flag, value| flag == !value.nil? },
      min: Proc.new { |bound, value| bound <= value },
      max: Proc.new { |bound, value| value <= bound },
      more_than: Proc.new { |bound, value| bound < value },
      less_than: Proc.new { |bound, value| value < bound },
      in: Proc.new { |list, value| list.include? value }
    }
  end

  def default_converter
    {
      String => {
        String => Proc.new { |value| value.to_s },
        Symbol => Proc.new { |value| value.to_sym },
        Integer => Proc.new { |value| value.to_i },
        Float => Proc.new { |value| value.to_f },
        Date => Proc.new { |value| Date.parse(value) },
        DateTime => Proc.new { |value| DateTime.parse(value) }
      },
      Integer => {
        Integer => Proc.new { |value| value.to_i },
        Float => Proc.new { |value| value.to_f },
        String => Proc.new { |value| value.to_s }
      },
      Float => {
        Float => Proc.new { |value| value.to_f },
        Integer => Proc.new { |value| value.to_i },
        String => Proc.new { |value| value.to_s }
      },
      TrueClass => {
        Boolean => Proc.new { |value| value }
      },
      FalseClass => {
        Boolean => Proc.new { |value| value }
      },
      Date => {
        Date => Proc.new { |value| value.to_date },
        DateTime => Proc.new { |value| value.to_datetime },
        Time => Proc.new { |value| value.to_time },
        String => Proc.new { |value| value.to_s }
      },
      DateTime => {
        DateTime => Proc.new { |value| value.to_datetime },
        Date => Proc.new { |value| value.to_date },
        Time => Proc.new { |value| value.to_time },
        String => Proc.new { |value| value.to_s }
      },
      Time => {
        Time => Proc.new { |value| value.to_time },
        DateTime => Proc.new { |value| value.to_datetime },
        Date => Proc.new { |value| value.to_date },
        String => Proc.new { |value| value.to_s }
      },
      Hash => {
        Array => Proc.new { |hash| hash.values }
      }
    }
  end

  extend self
end