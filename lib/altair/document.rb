require 'indifferent-hash'

module Altair
  class Document < IndifferentHash
    attr_accessor :fields
  
    def initialize(fields, data = IndifferentHash.new)
      @fields = fields
  
      fields.each do |key, field|
        case field
        when Hash
          define_singleton_method(:"#{key}") { get(key) }
          self[key] = self.class.new(field, data[key] || IndifferentHash.new)
        when Field
          define_singleton_method(:"#{key}") { get(key) }
          define_singleton_method(:"#{key}=") { |value| set(key, value) }
        end
      end

      apply(data)
    end

    def apply(data)
      data.each do |key, value|
        case fields[key]
        when Hash
          self[key].apply data[key]
        when Field
          set(key, value)
        end
      end
    end
  
    def get(key)
      self[key]
    end
  
    def set(key, value)
      self[key] = fields[key].parse! value
    end

    def validate! (options = {})
      fields.each do |key, field|
        case field
        when Hash
          self[key].validate! options
        when Field
          field.validate! self[key], options
        end
      end
    end
  end
end