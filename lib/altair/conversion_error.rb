module Altair
  class ConversionError < StandardError
    attr_reader :type, :value

    def initialize(type, value)
      @type = type
      @value = value
    end

    def message
      "Cannot convert `#{value}` (of class #{value.class}) to #{type}"
    end
  end
end