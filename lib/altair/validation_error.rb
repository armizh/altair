module Altair
  class ValidationError < StandardError
    attr_reader :validator, :value

    def initialize(validator, value)
      @validator = validator
      @value = value
    end

    def message
      "Validator :#{validator} failed against `#{value}` (of class #{value.class})"
    end
  end
end