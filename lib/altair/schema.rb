require 'altair'
require 'indifferent-hash'

module Altair
  class Schema < IndifferentHash
    def initialize(fields)
      Schema.parse fields, self
    end

    def self.parse(fields, result = IndifferentHash.new)
      fields.each do |key, field|
        result[key] = case field
        when Hash
          if field.has_key? :type
            Altair::Field.new(field)
          else
            parse field
          end
        else
          Altair::Field.new(type: field)
        end
      end

      result
    end
  end
end