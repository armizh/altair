require 'utils'
require 'altair/validation_error'
require 'altair/conversion_error'

module Altair
  class Field
    attr_accessor :type, :options
  
    def initialize(options)
      @type = options[:type]
      @options = options
    end
  
    def parse!(value, args = {})
      begin
        resolve_proc(Altair.converter[value.class][type], value)
      rescue
        raise ConversionError.new(type, value)
      end
    end

    def validate!(value, args = {})
      Altair.validators.each do |name, validator|
        if options.has_key? name
          raise ValidationError.new(name, value) unless validator.call(resolve_proc(options[name]), value)
        end
      end

      value
    end
  end
end