def resolve_proc(value, *args)
  case value
  when Proc
    value.call(*args)
  else
    value
  end
end
